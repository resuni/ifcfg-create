#!/bin/bash

## Generates new CentOS ifcfg-* network script for network interface specified by user input.
## Particularly useful when using virtual console interfaces where you can't copy/paste interface MAC addresses or UUID's. 

# Check for lack of arguments or lack of interface. Then make sure we don't overwrite any existing files.
if [ -z $1 ]; then
	echo "Please provide an interface name"
	exit 11
fi
if [ ! -d /sys/class/net/$1 ]; then
	printf "$1: No such interface\n"
	exit 12
fi
if [ -a /etc/sysconfig/network-scripts/ifcfg-$1 ]; then
	printf "ifcfg-$1 network script already exists. Please move/delete and try again\n"
	exit 13
fi

# Generate the file.
cd /etc/sysconfig/network-scripts/
printf "DEVICE=$1\n" >> ifcfg-$1
echo HWADDR=`cat /sys/class/net/$1/address` >> ifcfg-$1
echo TYPE=Ethernet >> ifcfg-$1
echo UUID=`uuidgen $1` >> ifcfg-$1
echo ONBOOT=yes >> ifcfg-$1
echo NM_CONTROLLED=yes >> ifcfg-$1
echo BOOTPROTO=dhcp >> ifcfg-$1

printf "New network script generated and saved at /etc/sysconfig/network-scripts/ifcfg-$1 \n"
exit 0
