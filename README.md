# ifcfg-create
Simple script that generates network scripts for RHEL-based operating systems

I recently discovered that RHEL/CentOS 7 includes this functionality with `nmcli` and `nmtui`. 

I'll leave this repository up, since RHEL 6 doesn't seem to have anything like this. 
